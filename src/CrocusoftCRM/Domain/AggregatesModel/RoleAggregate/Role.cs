﻿using Domain.AggregatesModel.UserAggregate;
using SharedKernel.Domain.Seedwork;

namespace Domain.AggregatesModel.RoleAggregate
{
    public class Role : Entity, IAggregateRoot
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public bool IsDeleted { get; private set; }

        private readonly List<User> _employee;
        public IReadOnlyCollection<User> Employees => _employee;

        public Role()
        {
            _employee = new List<User>();
        }

        public void SetDetails(string name, string description)
        {
            Name = name;
            Description = description;
        }
    }
}

﻿using Domain.AggregatesModel.ProjectAggregate;
using Domain.AggregatesModel.UserAggregate;
using SharedKernel.Domain.Seedwork;

namespace Domain.AggregatesModel.ReportAggregate
{
    public class Report : Editable<User>, IAggregateRoot
    {
        public string Text { get; private set; }
        public int ProjectId { get; private set; }
        public Project Project { get; private set; }
        public DateTime UpdateLimit { get; private set; }

        public void Set(int projectId, string text)
        {
            ProjectId = projectId;
            Text = text;
        }

        public Report()
        {
            UpdateLimit = DateTime.UtcNow.Date.AddHours(19).AddHours(1).AddSeconds(-1);
        }

        public void SetUpdateDate()
        {
            last_update_date = DateTime.UtcNow;
        }
    }
}
﻿using Domain.AggregatesModel.UserAggregate;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class Editable<TUser> : Auditable<TUser> where TUser : User
    {
        [ForeignKey("updated_by")]
        public int? updated_by_id { get; protected set; }
        public DateTime? last_update_date { get; protected set; }
        public TUser updated_by { get; protected set; }
        public void SetEditFields(int? updatedById)
        {
            updated_by_id = updatedById;
            last_update_date = DateTime.UtcNow;
        }
    }
}
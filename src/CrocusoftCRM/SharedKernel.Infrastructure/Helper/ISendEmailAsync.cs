﻿namespace SharedKernel.Infrastructure.Helper
{
    public interface ISendEmailAsync
    {
        Task SendEmailUserAsync(string email, string subject, string body, int? rowNumber);
        void SendEmailWithAttachmentsAsync(string email, string subject, string body, int? rowNumber, List<byte[]> attachments);

    }

}

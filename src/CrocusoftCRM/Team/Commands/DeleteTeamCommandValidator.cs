﻿using FluentValidation;

namespace TeamDetails.Commands
{
    public class DeleteTeamCommandValidator : AbstractValidator<DeleteTeamCommand>
    {
        public DeleteTeamCommandValidator() : base()
        {
            RuleFor(command => command.Id).NotNull();
        }
    }
}

﻿using Domain.AggregatesModel.TeamAggregate;
using Domain.Exceptions;
using Infrastructure.Commands;
using Infrastructure.Database;
using Infrastructure.Idempotency;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace TeamDetails.Commands
{
    public class UpdateTeamCommandHandler : IRequestHandler<UpdateTeamCommand, bool>
    {
        private readonly ITeamRepository _repo;
        private readonly ApplicationDbContext _context;

        public UpdateTeamCommandHandler(ITeamRepository repo, ApplicationDbContext context)
        {
            _repo = repo;
            _context = context;
        }

        public async Task<bool> Handle(UpdateTeamCommand request, CancellationToken cancellationToken)
        {
            Team? team = await _context.Teams.FirstOrDefaultAsync(t => t.Id == request.Id, cancellationToken)?? throw new DomainException("Entity could not be found");

            team.SetDetails(request.Name);
            _repo.UpdateAsync(team);
            await _repo.UnitOfWork.SaveChangesAsync(cancellationToken);
            return true;
        }
    }

    public class UpdateTeamIdentifiedCommandHandler : IdentifiedCommandHandler<UpdateTeamCommand, bool>
    {
        public UpdateTeamIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true; // Ignore duplicate requests
        }
    }
}

﻿using MediatR;

namespace TeamDetails.Commands
{
    public class DeleteTeamCommand : IRequest<bool>
    {
        public int Id { get; set; }
    }
}
﻿using MediatR;

namespace TeamDetails.Commands
{
    public class CreateTeamCommand : IRequest<bool>
    {
        public string Name { get; set; }
    }
}

﻿using AutoMapper;
using Domain.AggregatesModel.TeamAggregate;
using Identity.ViewModels;
using Infrastructure.Database;
using Microsoft.EntityFrameworkCore;
using SharedKernel.Domain.Seedwork;
using TeamDetails.ViewModels;

namespace TeamDetails.Queries
{
    public class TeamQueries : ITeamQueries
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public TeamQueries(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<TeamDto>> GetAllAsyncWithUsers()
        {
            var entities = await _context.Teams.Include(t => t.Users).ToListAsync();

            return _mapper.Map<IEnumerable<TeamDto>>(entities);
        }

        public async Task<IEnumerable<Team>> GetAllAsync()
        {
            var entities = await _context.Teams.ToListAsync();

            return _mapper.Map<IEnumerable<Team>>(entities);
        }

        public async Task<TeamDto> GetTeamById(int id)
        {
            var entity = await _context.Teams.Include(p => p.Users).FirstOrDefaultAsync(p => p.Id == id);

            if (entity == null) throw new ArgumentNullException();

            return _mapper.Map<TeamDto>(entity);
        }
    }
}
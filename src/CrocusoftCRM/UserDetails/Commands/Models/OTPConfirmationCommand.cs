﻿using MediatR;

namespace UserDetails.Commands.Models
{
    public class OTPConfirmationCommand : IRequest<bool>
    {
        public int OTP { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}

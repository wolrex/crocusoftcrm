﻿using FluentValidation;

namespace UserDetails.Commands.Models
{
    class ForgotPasswordCommandValidator : AbstractValidator<ForgotPasswordCommand>
    {
        public ForgotPasswordCommandValidator() : base()
        {
            RuleFor(command => command.Email).NotNull();
        }
    }
}

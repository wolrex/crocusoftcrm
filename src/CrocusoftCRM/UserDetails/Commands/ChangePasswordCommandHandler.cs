﻿using Domain.AggregatesModel.UserAggregate;
using Identity.Auth;
using Infrastructure.Commands;
using Infrastructure.Idempotency;
using SharedKernel.Infrastructure;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using UserDetails.Commands;

namespace UserDetails.Commands
{
    public class ChangePasswordCommandHandler : IRequestHandler<ChangePasswordCommand, bool>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserManager _userManager;

        public ChangePasswordCommandHandler(IUserRepository userRepository, IUserManager userManager)
        {
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        public async Task<bool> Handle(ChangePasswordCommand request, CancellationToken cancellationToken)
        {
            var user = await _userManager.GetCurrentUser();
            var oldPasswordHash = PasswordHasher.HashPassword(request.OldPassword);
            var newPasswordHash = PasswordHasher.HashPassword(request.NewPassword);
            user.SetPasswordHash(oldPasswordHash, newPasswordHash);
            _userRepository.UpdateAsync(user);
            await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return true;
        }
    }

    public class ChangePasswordIdentifiedCommandHandler : IdentifiedCommandHandler<ChangePasswordCommand, bool>
    {
        public ChangePasswordIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}

﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Domain.AggregatesModel.UserAggregate;
using Domain.Exceptions;
using Identity.Queries;
using Infrastructure.Commands;
using Infrastructure.Database;
using Infrastructure.Idempotency;
using SharedKernel.Infrastructure;
using System.Text.RegularExpressions;
using Domain.AggregatesModel.RoleAggregate;
using Microsoft.AspNetCore.Http;
using Identity.Auth;
using Domain.AggregatesModel.TeamAggregate;
using SharedKernel.Domain.Seedwork;

namespace UserDetails.Commands
{
    public class RegisterUserCommandHandler : IRequestHandler<RegisterUserCommand, bool>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserQueries _userQueries;
        private readonly ApplicationDbContext _context;
        private readonly IUserManager _userManager;

        public RegisterUserCommandHandler(IUserRepository userRepository, IUserQueries userQueries, ApplicationDbContext context, IUserManager userManager)
        {
            _userRepository = userRepository;
            _userQueries = userQueries;
            _context = context;
            _userManager = userManager;
        }

        public async Task<bool> Handle(RegisterUserCommand request, CancellationToken cancellationToken)
        {
            var self = await _userManager.GetCurrentUser();
            var email = request.Email;
            var existingUser = await _userQueries.FindByEmailAsync(email);

            if (existingUser != null)
            {
                throw new DomainException($"'{email}' is already taken, please choose another email.");
            }

            if (!IsValidEmail(email))
            {
                throw new DomainException($"{email} is not valid email. Domain must be crocusoft.com");
            }

            if (request.Password != request.ConfirmPassword)
            {
                throw new DomainException("Password do not match. Check and try again.");
            }

            Role role = await _context
                .Roles.FirstOrDefaultAsync(r => r.Id == request.RoleId, cancellationToken: cancellationToken)
                ?? throw new DomainException($"Role with id {request.RoleId} could not be found.");

            Team? team = request.TeamId.HasValue ?
            await _context.Teams.FirstOrDefaultAsync
            (t => t.Id == request.TeamId.Value, cancellationToken: cancellationToken)
            ?? throw new DomainException($"Team with id {request.TeamId} could not be found.")
            : null;

            if (self.Role.Name == CustomRole.Admin && request.RoleId == RoleParametr.Admin.Id || request.RoleId == RoleParametr.SuperAdmin.Id)
            {
                throw new DomainException("You do not have access to create admin or superadmin");
            }
            else if (self.Role.Name == CustomRole.SuperAdmin && request.RoleId == RoleParametr.SuperAdmin.Id)
            {
                throw new DomainException("You do not have access to create superadmin");
            }

            var user = new User(request.Email.Trim(), request.Phone.Trim(), PasswordHasher.HashPassword(request.Password), request.FirstName, request.LastName);

            if (role != null)
            {
                user.SetRole(role.Id);
            }

            user.SetTeam(team?.Id);

            await _userRepository.AddAsync(user);
            await _userRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
            return true;
        }

        const string emailRegex = @"^([a-zA-Z0-9_\-\.]+)@crocusoft\.com$";
        private static bool IsValidEmail(string email)
        {
            return Regex.IsMatch(email, emailRegex);
        }
    }

    public class RegisterUserIdentifiedCommandHandler :
        IdentifiedCommandHandler<RegisterUserCommand, bool>
    {
        public RegisterUserIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true; // Ignore duplicate requests
        }
    }
}

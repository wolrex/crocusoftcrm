﻿using Domain.AggregatesModel.RoleAggregate;
using Domain.AggregatesModel.TeamAggregate;
using Domain.AggregatesModel.UserAggregate;
using Domain.Exceptions;
using Identity.Auth;
using Identity.Queries;
using Infrastructure.Commands;
using Infrastructure.Database;
using Infrastructure.Idempotency;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SharedKernel.Domain.Seedwork;
using System.Data;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace UserDetails.Commands
{
    public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand, bool>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserQueries _userQueries;
        private readonly ApplicationDbContext _context;
        private readonly IUserManager _userManager;

        public UpdateUserCommandHandler(IUserRepository userRepository, IUserQueries userQueries, ApplicationDbContext context, IUserManager userManager)
        {
            _userRepository = userRepository;
            _userQueries = userQueries;
            _context = context;
            _userManager = userManager;
            _userManager = userManager;
        }

        public async Task<bool> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            var email = request.Email;
            var user = await _userQueries.GetUserEntityAsync(request.Id) ?? throw new DomainException($"User with id {request.Id} not found."); ;

            var self = await _userManager.GetCurrentUser();

            if (self.Role.Name == CustomRole.Admin && (request.RoleId == RoleParametr.Head.Id || request.RoleId == RoleParametr.Employee.Id))
            {
                if (user.Role.Name == CustomRole.Admin) // Downgrading an admin
                {
                    throw new DomainException("You do not have access to downgrade an admin user's role.");
                }
            }
            else if (self.Role.Name == CustomRole.Admin && (request.RoleId == RoleParametr.Admin.Id || request.RoleId == RoleParametr.Head.Id || request.RoleId == RoleParametr.Employee.Id))
            {
                if (user.Role.Name == CustomRole.SuperAdmin) // Downgrading a superadmin
                {
                    throw new DomainException("You do not have access to downgrade a superadmin user's role.");
                }
            }

            if (self.Role.Name == CustomRole.Admin && request.RoleId == RoleParametr.Admin.Id || request.RoleId == RoleParametr.SuperAdmin.Id)
            {
                throw new DomainException("You do not have access to update user's role to admin or superadmin");
            }
            else if (self.Role.Name == CustomRole.SuperAdmin && request.RoleId == RoleParametr.SuperAdmin.Id)
            {
                throw new DomainException("You do not have access to update user's role to superadmin");
            }

            Role role = await _context.Roles.FirstOrDefaultAsync
                (r => r.Id == request.RoleId, cancellationToken: cancellationToken)
            ?? throw new DomainException($"Role with id {request.RoleId} could not be found.");

            Team? team = request.TeamId.HasValue ? await _context.Teams.FirstOrDefaultAsync
                (t => t.Id == request.TeamId, cancellationToken: cancellationToken)
            ?? throw new DomainException($"Team with id {request.TeamId} could not be found.") :
            null;

            if (request.Email != user.Email)
            {
                var requestEmail = request.Email;
                var existingUser = await _userQueries.FindByNameAsync(requestEmail);

                if (existingUser != null)
                {
                    throw new DomainException($"Email '{requestEmail}' already taken, please choose another email.");
                }
            }

            if (!IsValidEmail(email))
            {
                throw new DomainException($"{email} is not valid email. Domain must be crocusoft.com");
            }

            user.SetDetails(request.Email.Trim(), request.Phone.Trim(), request.FirstName, request.LastName);

            user.SetRole(request.RoleId);
            user.SetTeam(request.TeamId);

            _userRepository.UpdateAsync(user);
            await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return true;
        }

        const string emailRegex = @"^([a-zA-Z0-9_\-\.]+)@crocusoft\.com$";
        private static bool IsValidEmail(string email)
        {
            return Regex.IsMatch(email, emailRegex);
        }
    }

    public class UpdateUserIdentifiedCommandHandler : IdentifiedCommandHandler<UpdateUserCommand, bool>
    {
        public UpdateUserIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true; // Ignore duplicate requests
        }
    }
}

﻿using MediatR;

namespace UserDetails.Commands
{
    public class CheckTokenVerifyInputCommand : IRequest<bool>
    {
        public string Token { get; set; }

        public string UserName { get; set; }
    }
}

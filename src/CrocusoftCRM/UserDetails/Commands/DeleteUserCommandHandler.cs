﻿using Domain.AggregatesModel.UserAggregate;
using Identity.Auth;
using Identity.Queries;
using Infrastructure.Commands;
using Infrastructure.Idempotency;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace UserDetails.Commands
{
    public class DeleteUserCommandHandler : IRequestHandler<DeleteUserCommand, bool>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserQueries _userQueries;

        public DeleteUserCommandHandler(IUserQueries userQueries, IUserRepository userRepository)
        {
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _userQueries = userQueries ?? throw new ArgumentNullException(nameof(userQueries));
        }

        public async Task<bool> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            foreach (var id in request.Id)
            {
                var user = await _userQueries.FindAsync(id);

                user.Delete();
                _userRepository.UpdateAsync(user);
            }

            await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return true;
        }
    }

    public class DeleteUserIdentifiedCommandHandler : IdentifiedCommandHandler<DeleteUserCommand, bool>
    {
        public DeleteUserIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}
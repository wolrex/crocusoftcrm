﻿using MediatR;
using Domain.AggregatesModel.UserAggregate;
using Identity.Auth;
using Identity.Queries;
using Infrastructure.Commands;
using Infrastructure.Idempotency;

namespace UserDetails.Commands
{
    public class ActiveUserCommandHandler : IRequestHandler<ActiveUserCommand, bool>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserQueries _userQueries;

        public ActiveUserCommandHandler(IUserQueries userQueries, IUserManager userManager, IUserRepository userRepository)
        {
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _userQueries = userQueries ?? throw new ArgumentNullException(nameof(userQueries));
        }

        public async Task<bool> Handle(ActiveUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _userQueries.FindAsync(request.Id);

            user.SetActivated(request.IsActive);
            _userRepository.UpdateAsync(user);
            await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return true;
        }
    }

    public class ActiveUserIdentifiedCommandHandler : IdentifiedCommandHandler<ActiveUserCommand, bool>
    {
        public ActiveUserIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}

﻿using Domain.AggregatesModel.ProjectAggregate;
using ProjectDetails.ViewModels;
using SharedKernel.Infrastructure.Queries;

namespace ProjectDetails.Queries
{
    public interface IProjectQueries : IQuery
    {
        Task<IEnumerable<ProjectDtoForGet>> GetAllAsyncWithUsers();
        Task<IEnumerable<ProjectDtoForCurrentUser>> GetProjectsWithCurrentUser();
        Task<IEnumerable<Project>> GetAllAsync();
        Task<ProjectDtoForGet> GetProjectById(int id);
        Task<IEnumerable<ProjectDtoForGet>> SearchProjects(string searchTerm);
    }
}

﻿using AutoMapper;
using Domain.AggregatesModel.ProjectAggregate;
using Identity.Auth;
using Infrastructure.Database;
using Microsoft.EntityFrameworkCore;
using ProjectDetails.ViewModels;

namespace ProjectDetails.Queries
{
    public class ProjectQueries : IProjectQueries
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUserManager _userManager;

        public ProjectQueries(ApplicationDbContext context, IMapper mapper, IUserManager userManager)
        {
            _context = context;
            _mapper = mapper;
            _userManager = userManager;
        }
        public async Task<IEnumerable<Project>> GetAllAsync()
        {
            var entities = await _context.Projects.ToListAsync();
            return _mapper.Map<IEnumerable<Project>>(entities);
        }
        public async Task<IEnumerable<ProjectDtoForGet>> GetAllAsyncWithUsers()
        {
            var projectsWithUsers = 
                await _context.Projects
                .Include(p => p.ProjectUser)
                .ThenInclude(pu => pu.User)
                .ToListAsync();

            var projectDtos = projectsWithUsers.Select(project =>
            {
                var projectDto = _mapper.Map<ProjectDtoForGet>(project);
                projectDto.Users = project.ProjectUser
                    .Select(pu => _mapper.Map<UserDtoForProject>(pu.User))
                    .ToList();
                return projectDto;
            });

            return projectDtos;
        }
        public async Task<ProjectDtoForGet> GetProjectById(int id)
        {
            var project = 
                await _context.Projects
                .Include(p => p.ProjectUser)
                .ThenInclude(pu => pu.User)
                .FirstOrDefaultAsync(p => p.Id == id);

            if (project == null)
            {
                throw new Exception("No project found matching the id you provided!");
            }

            var projectDto = _mapper.Map<ProjectDtoForGet>(project);
            projectDto.Users = project.ProjectUser
                .Select(pu => _mapper.Map<UserDtoForProject>(pu.User))
                .ToList();

            return projectDto;
        }

        public async Task<IEnumerable<ProjectDtoForCurrentUser>> GetProjectsWithCurrentUser()
        {
            var currentUser = await _userManager.GetCurrentUser()
                ?? throw new InvalidOperationException("User could not be found");

            //if (currentUser == null)
            //{
            //    throw new InvalidOperationException("Current user not found.");
            //}

            var projects = await _context.Projects
                .Where(p => p.ProjectUser.Any(u => u.UserId == currentUser.Id))
                .ToListAsync();

            return _mapper.Map<IEnumerable<ProjectDtoForCurrentUser>>(projects);
        }

        public async Task<IEnumerable<ProjectDtoForGet>> SearchProjects(string searchTerm)
        {
            var searchTermLower = searchTerm.ToLower();

            var projects = await _context.Projects
                .Include(p => p.ProjectUser)
                    .ThenInclude(pu => pu.User)
                .Where(p => p.Name.ToLower().Contains(searchTermLower))
                .ToListAsync();

            var projectDtos = projects.Select(project =>
            {
                var projectDto = _mapper.Map<ProjectDtoForGet>(project);
                projectDto.Users = project.ProjectUser
                    .Select(pu => _mapper.Map<UserDtoForProject>(pu.User))
                    .ToList();
                return projectDto;
            });

            return projectDtos;
        }

    }
}
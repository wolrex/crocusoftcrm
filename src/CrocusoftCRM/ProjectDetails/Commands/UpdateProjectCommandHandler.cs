﻿using Domain.AggregatesModel.ProjectAggregate;
using Domain.AggregatesModel.UserAggregate;
using Domain.Exceptions;
using Infrastructure.Commands;
using Infrastructure.Database;
using Infrastructure.Idempotency;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace ProjectDetails.Commands
{
    public class UpdateProjectCommandHandler : IRequestHandler<UpdateProjectCommand, bool>
    {
        private readonly IProjectRepository _projectRepo;
        private readonly IUserRepository _userRepo;
        private readonly ApplicationDbContext _context;

        public UpdateProjectCommandHandler(IProjectRepository projectRepo, IUserRepository userRepo, ApplicationDbContext context)
        {
            _projectRepo = projectRepo;
            _userRepo = userRepo;
            _context = context;
        }

        public async Task<bool> Handle(UpdateProjectCommand request, CancellationToken cancellationToken)
        {
            var project = await _context.Projects.FirstOrDefaultAsync(p=> p.Id == request.ProjectId) ;

            if (!string.IsNullOrEmpty(request.Name))
            {
                project.SetDetails(request.Name);
            }
            
            foreach (int userId in request.UserIdsToDelete)
            {
                var projectUser = await _context.ProjectUser.FirstOrDefaultAsync(pu => pu.UserId == userId && pu.ProjectId == request.ProjectId, cancellationToken: cancellationToken);
                if (projectUser != null)
                {
                    _context.ProjectUser.Remove(projectUser);
                }
                else
                {
                    throw new DomainException("the user couldn't be found in the project");
                }
            }

            if (request.UserIdsToAdd != null)
            {
                foreach (int userId in request.UserIdsToAdd)
                {
                    var existingUser = await _context.ProjectUser.FirstOrDefaultAsync(pu => pu.UserId == userId && pu.ProjectId == request.ProjectId, cancellationToken: cancellationToken);
                    if (existingUser == null)
                    {
                        var data = new ProjectUser
                        {
                            UserId = userId,
                            ProjectId = request.ProjectId,
                        };
                        await _context.ProjectUser.AddAsync(data, cancellationToken);
                    }
                    else
                    {
                        throw new DomainException("This user already registered to this project");
                    }
                }
            }

            await _context.SaveChangesAsync(cancellationToken);

            return true;
        }
    }

    public class UpdateProjectIdentifiedCommandHandler : IdentifiedCommandHandler<UpdateProjectCommand, bool>
    {
        public UpdateProjectIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}

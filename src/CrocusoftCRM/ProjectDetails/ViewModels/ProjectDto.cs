﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDetails.ViewModels
{
    public class ProjectDto
    {
        public string Name { get; set; }
        public List<int> UserIds { get; set; }
    }
}

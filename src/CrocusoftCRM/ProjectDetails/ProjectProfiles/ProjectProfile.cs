﻿using AutoMapper;
using Domain.AggregatesModel.ProjectAggregate;
using Domain.AggregatesModel.TeamAggregate;
using Domain.AggregatesModel.UserAggregate;
using ProjectDetails.Commands;
using ProjectDetails.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDetails.ProjectProfiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<ProjectDto, Project>();
            CreateMap<Project, ProjectDto>();

            CreateMap<CreateProjectCommand, Project>()
            .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name));

            CreateMap<Project, ProjectDtoForGet>()
             .ForMember(dest => dest.Users, opt => opt.MapFrom(src => src.ProjectUser));

            CreateMap<User, UserDtoForProject>()
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName));

            CreateMap<ProjectUser, UserDtoForProject>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.User.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.User.LastName));

            CreateMap<Project, ProjectDtoForCurrentUser>();
                 //.ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UserId))
                 //.ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Project.Name));
        }
    }
}

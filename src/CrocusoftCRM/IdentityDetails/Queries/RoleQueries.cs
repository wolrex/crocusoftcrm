﻿using AutoMapper;
using Domain.AggregatesModel.RoleAggregate;
using Identity.ViewModels;
using Infrastructure.Database;
using Microsoft.EntityFrameworkCore;

namespace Identity.Queries
{
    public class RoleQueries : IRoleQueries
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public RoleQueries(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<RoleDto>> GetAllAsync()
        {
            var entities = await _context.Roles.ToListAsync();

            return _mapper.Map<IEnumerable<RoleDto>>(entities);
        }

        public async Task<Role> GetByName(RoleParametr role)
        {
            var entity = await _context.Roles.FirstOrDefaultAsync(p => p.Name == role.Name);

            return entity;
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Infrastructure.Idempotency;

namespace Infrastructure.EntityConfigurations.AuditEntityConfiguration
{
    internal class ClientRequestEntityTypeConfiguration
    : IEntityTypeConfiguration<ClientRequest>
    {
        public void Configure(EntityTypeBuilder<ClientRequest> requestConfiguration)
        {
            requestConfiguration.ToTable("requests");

            requestConfiguration.HasKey(cr => cr.Id);
            requestConfiguration.Property(cr => cr.Id).HasColumnName("id");
            requestConfiguration.HasIndex(cr => cr.Key).IsUnique();
            requestConfiguration.Property(cr => cr.Key).HasColumnName("key");
            requestConfiguration.Property(cr => cr.Name).IsRequired().HasColumnName("name");
            requestConfiguration.Property(cr => cr.Time).HasColumnName("time");
        }
    }
}

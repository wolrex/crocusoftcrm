﻿using Domain.AggregatesModel.ReportAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityConfigurations.ReportEntityConfiguration
{
    public class ReportEntityTypeConfiguration : IEntityTypeConfiguration<Report>
    {
        public void Configure(EntityTypeBuilder<Report> configuration)
        {
            configuration.ToTable("reports");
            configuration.HasKey(x => x.Id);
            configuration.Property(o => o.Id).HasColumnName("id");
            configuration.Property(o => o.Text).HasMaxLength(1000).IsRequired().HasColumnName("text");
            configuration.Property(o => o.record_date).IsRequired().HasColumnName("created_date");
            configuration.Property(o => o.UpdateLimit).IsRequired().HasColumnName("update_limit_date");
            configuration.Property(o => o.last_update_date).HasColumnName("update_date");

            //Ignore
            configuration.Ignore(o => o.updated_by);
            configuration.Ignore(o => o.updated_by_id);

            configuration
                .HasOne(o => o.created_by)
                .WithMany(u => u.Reports)
                .HasForeignKey(o => o.created_by_id);

            configuration
                .HasOne(o => o.Project)
                .WithMany(p => p.Reports)
                .HasForeignKey(o => o.ProjectId);

        }
    }
}

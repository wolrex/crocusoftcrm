﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Domain.AggregatesModel.TeamAggregate;

namespace Infrastructure.EntityConfigurations.TeamEntityConfiguration
{
    public class TeamEntityTypeConfiguration : IEntityTypeConfiguration<Team>
    {
        public void Configure(EntityTypeBuilder<Team> teamConf)
        {
            teamConf.ToTable("teams");
            teamConf.HasKey(o => o.Id);
            teamConf.Property(o => o.Id).HasColumnName("id");
            teamConf.Property(o => o.Name).IsRequired().HasColumnName("name");
        }
    }
}
﻿using Domain.AggregatesModel.UserAggregate;
using Infrastructure.Database;
using SharedKernel.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public sealed override DbContext Context { get; protected set; }

        public UserRepository(ApplicationDbContext context)
        {
            Context = context;
        }
    }
}

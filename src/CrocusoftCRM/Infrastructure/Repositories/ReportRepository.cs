﻿using Domain.AggregatesModel.ProjectAggregate;
using Domain.AggregatesModel.ReportAggregate;
using Infrastructure.Database;
using Microsoft.EntityFrameworkCore;
using SharedKernel.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class ReportRepository : Repository<Report>, IReportRepository
    {
        public sealed override DbContext Context { get; protected set; }

        public ReportRepository(ApplicationDbContext context)
        {
            Context = context;
        }
    }
}

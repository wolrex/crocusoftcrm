﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Domain.AggregatesModel.RoleAggregate;
using Domain.AggregatesModel.UserAggregate;
using Infrastructure.EntityConfigurations.AuditEntityConfiguration;
using Infrastructure.EntityConfigurations.IdentityEntityConfiguration;
using Infrastructure.EntityConfigurations.RoleEntityConfiguration;
using SharedKernel.Domain.Seedwork;
using SharedKernel.Infrastructure;
using Domain.AggregatesModel.TeamAggregate;
using Domain.AggregatesModel.ProjectAggregate;
using Infrastructure.EntityConfigurations.TeamEntityConfiguration;
using Infrastructure.EntityConfigurations.ProjectConfiguration;
using Infrastructure.EntityConfigurations.ReportEntityConfiguration;
using Domain.AggregatesModel.ReportAggregate;

namespace Infrastructure.Database
{
    public sealed class ApplicationDbContext : DbContext, IUnitOfWork
    {
        public const string IDENTITY_SCHEMA = "Identity";
        public const string DEFAULT_SCHEMA = "dbo";
        private readonly IMediator _mediator;

        public ApplicationDbContext(DbContextOptions options, IMediator mediator) : base(options)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        public DbSet<User> Users { get; private set; }
        public DbSet<Role> Roles { get; private set; }
        public DbSet<Team> Teams { get; private set; }
        public DbSet<Project> Projects { get; private set; }
        public DbSet<ProjectUser> ProjectUser { get; private set; }
        public DbSet<Report> Reports { get; private set; }


        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            await _mediator.DispatchDomainEventsAsync(this);

            await SaveChangesAsync(true, cancellationToken);

            return true;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ClientRequestEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UserEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RoleEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TeamEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ProjectEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ReportEntityTypeConfiguration());

            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.ClientSetNull;
            }
        }
    }
}
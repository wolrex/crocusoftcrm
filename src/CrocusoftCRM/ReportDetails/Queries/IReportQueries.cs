﻿using Domain.AggregatesModel.ReportAggregate;
using ReportDetails.DTO;
using ReportDetails.Response;
using ReportDetails.ViewModels;
using SharedKernel.Infrastructure.Queries;

namespace ReportDetails.Queries
{
    public interface IReportQueries : IQuery
    {
        Task<IEnumerable<ReportDto>> GetAllAsync();
        Task<IEnumerable<ReportDto>> GetReportsForCurrentUserAsync();
        Task<ReportDto> GetReportById(int id);
        Task<ReportDto> GetReportByIdForCurrentUserAsync(int id);
        Task<IEnumerable<Report>> SearchReport(string searchTerm);
        public Task<AllReportsResponse> GetDailyReportFilter(DailyReportFilterDTO request);
        public Task<AllReportsResponse> GetUserReportFilter(GetSelfDailyReportsDTO request);
    }
}
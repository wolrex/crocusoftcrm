﻿using AutoMapper;
using Domain.AggregatesModel.ReportAggregate;
using Domain.AggregatesModel.UserAggregate;
using Identity.Auth;
using Infrastructure.Database;
using Microsoft.EntityFrameworkCore;
using NotFound.Exceptions;
using ReportDetails.DTO;
using ReportDetails.Response;
using ReportDetails.ViewModels;
using System.Linq.Dynamic.Core;

namespace ReportDetails.Queries
{
    public class ReportQueries : IReportQueries
    {
        private readonly ApplicationDbContext _context;
        private readonly IUserManager _userManager;
        private readonly IMapper _mapper;

        public ReportQueries(ApplicationDbContext context, IMapper mapper, IUserManager userManager)
        {
            _context = context;
            _mapper = mapper;
            _userManager = userManager;
        }

        public async Task<IEnumerable<ReportDto>> GetAllAsync()
        {
            var reportsWithUsers = await _context.Reports
                .Include(r => r.created_by)
                .Include(r => r.Project)
                .ToListAsync();

            return _mapper.Map<IEnumerable<ReportDto>>(reportsWithUsers);
        }

        public async Task<IEnumerable<ReportDto>> GetReportsForCurrentUserAsync()
        {
            int self = _userManager.GetCurrentUserId();

            var reportsWithUsers = await _context.Reports
                .Include(r => r.created_by)
                .Include(r => r.Project)
                .Where(r => r.created_by.Id == self)
                .ToListAsync();

            return _mapper.Map<IEnumerable<ReportDto>>(reportsWithUsers);
        }

        public async Task<ReportDto> GetReportById(int reportId)
        {
            var report = await _context.Reports
                .Include(r => r.created_by)
                .Include(r => r.Project)
                .FirstOrDefaultAsync(r => r.Id == reportId);

            if (report == null)
            {
                throw new NotFoundException($"Report with ID {reportId} not found.");
            }

            return _mapper.Map<ReportDto>(report);
        }

        public async Task<ReportDto> GetReportByIdForCurrentUserAsync(int reportId)
        {
            int self = _userManager.GetCurrentUserId();

            var report = await _context.Reports
                .Include(r => r.created_by)
                .Include(r => r.Project)
                .FirstOrDefaultAsync(r => r.Id == reportId && r.created_by.Id == self);

            if (report == null)
            {
                throw new NotFoundException($"Report with ID {reportId} not found or does not belong to the current user.");
            }

            return _mapper.Map<ReportDto>(report);
        }


        public Task<IEnumerable<Report>> SearchReport(string searchTerm)
        {
            throw new NotImplementedException();
        }

        public async Task<AllReportsResponse> GetDailyReportFilter(DailyReportFilterDTO request)
        {
            var entities = _context.Reports
                .Include(d => d.created_by)
                .Include(d => d.Project)
                .AsQueryable();

            entities = entities.Where(p =>
                (request.ProjectIds == null || !request.ProjectIds.Any() || request.ProjectIds.Contains(p.Project.Id)) &&
                (request.UserIds == null || !request.UserIds.Any() || request.UserIds.Contains(p.created_by_id)) &&
                (!request.CreateDate.HasValue || p.record_date.Date == request.CreateDate.Value.Date) &&
                (!request.UpdateDate.HasValue || (p.last_update_date != null && p.last_update_date.Value.Date == request.UpdateDate.Value.Date)) &&
                (!request.StartDate.HasValue || (p.record_date.Date >= request.StartDate.Value.Date)) &&
                (!request.EndDate.HasValue || (p.record_date.Date <= request.EndDate.Value.Date))
            );

            var count = (await entities.ToListAsync()).Count;
            if (request.LoadMore?.SortField != null)
            {
                if (request.LoadMore.OrderBy)
                {
                    entities = entities.OrderBy($"p=>p.{request.LoadMore.SortField}");
                }
                else
                {
                    entities = entities.OrderBy($"p=>p.{request.LoadMore.SortField} descending");
                }
            }

            if (request.LoadMore?.Skip != null && request.LoadMore?.Take != null)
            {
                entities = entities.Skip(request.LoadMore.Skip.Value).Take(request.LoadMore.Take.Value);
            }

            var mapped = _mapper.Map<IEnumerable<DailyReportResponse>>(entities);
            AllReportsResponse outputModel = new()
            {
                Data = mapped,
                TotalCount = count
            };
            return outputModel;
        }

        public async Task<AllReportsResponse> GetUserReportFilter(GetSelfDailyReportsDTO request)
        {
            var self = await _userManager.GetCurrentUser();
            var query = _context
                .Reports
                .Include(r => r.Project)
                .Where(p => p.created_by_id == self.Id)
                .AsQueryable();

            query = query.Where(p =>
                (request.StartDate == null || p.record_date.Date >= request.StartDate) &&
                (request.EndDate == null || p.record_date.Date <= request.EndDate));

            int count = await query.CountAsync();

            if (request.LoadMore.SortField != null)
            {
                if (!string.IsNullOrEmpty(request.LoadMore.SortField))
                {
                    if (request.LoadMore.OrderBy)
                    {
                        query = query.OrderBy($"p=>p.{request.LoadMore.SortField}");
                    }
                    else
                    {
                        query = query.OrderBy($"p=>p.{request.LoadMore.SortField} descending");
                    }
                }

                if (request.LoadMore.Skip != null && request.LoadMore.Take != null)
                {
                    query = query.Skip(request.LoadMore.Skip.Value).Take(request.LoadMore.Take.Value);
                }
            }
            var entities = await query.ToListAsync();
            var mapped = _mapper.Map<IEnumerable<DailyReportResponse>>(entities);

            AllReportsResponse outputModel = new()
            {
                Data = mapped,
                TotalCount = count
            };

            return outputModel;
        }
    }
}

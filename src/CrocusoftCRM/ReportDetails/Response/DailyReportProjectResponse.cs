﻿namespace ReportDetails.Response
{
    public class DailyReportProjectResponse
    {
        public int Id { get; set; }
        public string Title { get; private set; }
    }
}
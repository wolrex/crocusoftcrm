﻿

using Domain.AggregatesModel.ProjectAggregate;
using Domain.AggregatesModel.ReportAggregate;
using Domain.Exceptions;
using Identity.Auth;
using Infrastructure.Commands;
using Infrastructure.Database;
using Infrastructure.Idempotency;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;

namespace ReportDetails.Commands
{
    public class CreateReportCommandHandler : IRequestHandler<CreateReportCommand, bool>
    {
        private readonly IUserManager _userManager;
        private readonly IReportRepository _reportRepository;
        private readonly ApplicationDbContext _context;

        public CreateReportCommandHandler(IUserManager userManager, IReportRepository reportRepository, ApplicationDbContext context)
        {
            _userManager = userManager;
            _reportRepository = reportRepository;
            _context = context;
        }

        public async Task<bool> Handle(CreateReportCommand request, CancellationToken cancellationToken)
        {
            int self = _userManager.GetCurrentUserId();

            ProjectUser? userProject = await _context
                .ProjectUser
                .FirstOrDefaultAsync(up => up.UserId == self && up.ProjectId == request.ProjectId, cancellationToken: cancellationToken)
                ?? throw new DomainException("User is not associated with this project.");

            Report newReport = new();
            newReport.Set(request.ProjectId, request.Text);
            newReport.SetAuditFields(self);
            await _reportRepository.AddAsync(newReport);
            await _reportRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return true;
        }

        public class RegisterCreateReportIdentifiedCommandHandler : IdentifiedCommandHandler<CreateReportCommand, bool>
        {
            public RegisterCreateReportIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
            {
            }

            protected override bool CreateResultForDuplicateRequest()
            {
                return true;
            }
        }
    }
}

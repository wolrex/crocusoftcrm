﻿using Domain.AggregatesModel.ProjectAggregate;
using Domain.AggregatesModel.ReportAggregate;
using Domain.AggregatesModel.UserAggregate;
using Domain.Exceptions;
using Identity.Auth;
using Infrastructure.Commands;
using Infrastructure.Database;
using Infrastructure.Idempotency;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ReportDetails.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportDetails.Commands
{
    public class UpdateReportCommandHandler : IRequestHandler<UpdateReportCommand, bool>
    {
        private readonly IReportRepository _reportRepo;
        private readonly IUserRepository _userRepo;
        private readonly IUserManager _userManager;
        private readonly IReportQueries _reportQueries;
        private readonly ApplicationDbContext _context;

        public UpdateReportCommandHandler(IReportRepository reportRepo, IUserRepository userRepo, ApplicationDbContext context, IUserManager userManager, IReportQueries reportQueries)
        {
            _reportRepo = reportRepo;
            _userRepo = userRepo;
            _userManager = userManager;
            _reportQueries = reportQueries;
            _context = context;
        }

        public async Task<bool> Handle(UpdateReportCommand request, CancellationToken cancellationToken)
        {
            Report? data = await _reportRepo.GetAsync(request.Id);
            int? selfId = _userManager.GetCurrentUserId();

            if (data.created_by_id != selfId)
            {
                throw new UnauthorizedAccessException("You are not authorized to update this project.");
            }
            if (data.UpdateLimit.Date <= DateTime.UtcNow.Date)
            {
                throw new UnauthorizedAccessException($"Access to update this report has passed. Limit: {data.UpdateLimit.Date}");
            }

            ProjectUser? userProject = await _context
                .ProjectUser
                .FirstOrDefaultAsync(up => up.ProjectId == request.ProjectId, cancellationToken: cancellationToken)
                ?? throw new DomainException("User is not associated with this project.");

            data.Set(request.ProjectId, request.Text);
            data.SetUpdateDate();
            _reportRepo.UpdateAsync(data);
            await _reportRepo.UnitOfWork.SaveChangesAsync(cancellationToken);
            return true;
        }
    }

    public class UpdateReportIdentifiedCommandHandler : IdentifiedCommandHandler<UpdateReportCommand, bool>
    {
        public UpdateReportIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}

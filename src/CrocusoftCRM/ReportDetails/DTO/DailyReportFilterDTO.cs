﻿using Infrastructure.Constants;

namespace ReportDetails.DTO
{
    public class DailyReportFilterDTO
    {
        public List<int>? ProjectIds { get; set; }
        public List<int>? UserIds { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public LoadMoreDto? LoadMore { get; set; }
    }
}

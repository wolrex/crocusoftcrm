﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Polly;
using Polly.Retry;
using System.Data.SqlClient;
using Domain.AggregatesModel.RoleAggregate;
using Domain.AggregatesModel.UserAggregate;
using Infrastructure.Database;
using SharedKernel.Infrastructure;

namespace Infrastructure.DBSeed
{
    public class ApplicationDbContextSeed
    {
        public async Task SeedAsync(
            ApplicationDbContext context,
            IWebHostEnvironment env,
            IOptions<CrocusoftCRMSettings> settings,
            ILogger<ApplicationDbContextSeed> logger)
        {
            var policy = CreatePolicy(logger, nameof(ApplicationDbContextSeed));

            using (context)
            {
                await policy.ExecuteAsync(async () =>
                {
                    await context.SaveChangesAsync();

                    var systemUser = await context.Users.FirstOrDefaultAsync();

                    if (systemUser == null)
                    {
                        #region Role Generation
                        #region Super Admin Role
                        var superAdminRole = new Role();
                        superAdminRole.SetDetails(RoleParametr.SuperAdmin.Name, "Super Admin");
                        await context.Roles.AddAsync(superAdminRole);
                        #endregion
                        #region Admin Role
                        var adminRole = new Role();
                        adminRole.SetDetails(RoleParametr.Admin.Name, "Admin");
                        await context.Roles.AddAsync(adminRole);
                        #endregion
                        #region Employee Role
                        var employeeRole = new Role();
                        employeeRole.SetDetails(RoleParametr.Employee.Name, "Employee");
                        await context.Roles.AddAsync(employeeRole);
                        #endregion
                        #region Head Role
                        Role? headRole = new();
                        headRole.SetDetails(RoleParametr.Head.Name, "Head");
                        await context.Roles.AddAsync(headRole);
                        #endregion
                        await context.SaveChangesAsync();
                        #endregion
                        #region User Generation
                        User user = new User("superadmin@crocusoft.com", "+994516975544", PasswordHasher.HashPassword("Test123!"), "Idris", "Mikayil");
                        user.SetRole(superAdminRole.Id);
                        await context.Users.AddAsync(user);
                        #endregion
                        await context.SaveChangesAsync();
                    }
                });
            }
        }

        private AsyncRetryPolicy CreatePolicy(ILogger<ApplicationDbContextSeed> logger, string prefix, int retries = 3)
        {
            return Policy.Handle<SqlException>().WaitAndRetryAsync(
                retries,
                retry => TimeSpan.FromSeconds(5),
                (exception, timeSpan, retry, ctx) =>
                {
                    logger.LogTrace(
                        $"[{prefix}] Exception {exception.GetType().Name} with message ${exception.Message} detected on attempt {retry} of {retries}");
                }
            );
        }
    }
}


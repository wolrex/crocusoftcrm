﻿using Extensions;
using Infrastructure;
using Infrastructure.Database;
using Infrastructure.DBSeed;
using Microsoft.AspNetCore;
using Microsoft.Extensions.Options;
using Serilog;

namespace CrocusoftCRM
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            Log.Logger = new LoggerConfiguration()
            .WriteTo.File("logs/log.txt", rollingInterval: RollingInterval.Day)
            .WriteTo.Seq("http://localhost:5341")
            .CreateLogger();

            host.MigrateDbContext<ApplicationDbContext>(async (context, services) =>
            {
                var env = services.GetService<IWebHostEnvironment>();
                var settings = services.GetService<IOptions<CrocusoftCRMSettings>>();
                var logger = services.GetService<ILogger<ApplicationDbContextSeed>>();

                var seeder = new ApplicationDbContextSeed();
                await seeder.SeedAsync(context, env, settings, logger);
            });

            host.Run();
        }

        public static IWebHostBuilder CreateHostBuilder(string[] args) =>
        WebHost.CreateDefaultBuilder(args).ConfigureServices((hostContext, services) =>
        {

        }).UseSerilog().UseStartup<Startup>();
    }
}

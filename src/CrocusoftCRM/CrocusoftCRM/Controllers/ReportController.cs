﻿using CustomAuthorizeLogic;
using Extensions;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using ReportDetails.Commands;
using ReportDetails.DTO;
using ReportDetails.Queries;
using ReportDetails.ViewModels;
using SharedKernel.Domain.Seedwork;

namespace CrocusoftCRM.Controllers
{
    [Route("report")]
    public class ReportController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IReportQueries _reportQueries;

        public ReportController(IMediator mediator, IReportQueries reportQueries)
        {
            _mediator = mediator ?? throw new ArgumentException(nameof(mediator));
            _reportQueries = reportQueries ?? throw new ArgumentNullException(nameof(reportQueries));
        }

        [AuthorizeRoles(CustomRole.Employee)]
        [HttpPost("create")]
        public async Task<IActionResult> CreateReport([FromBody] CreateReportCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<CreateReportCommand, bool>(command, requestId);
            return NoContent();
        }

        [AuthorizeRoles(CustomRole.Employee)]
        [HttpPut("update")]
        public async Task<IActionResult> UpdateReport([FromBody] UpdateReportCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<UpdateReportCommand, bool>(command, requestId);
            return NoContent();
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head)]
        [HttpGet("AllReports")]
        public async Task<IEnumerable<ReportDto>> GetAllAsync()
        {
            return await _reportQueries.GetAllAsync();
        }

        [AuthorizeRoles(CustomRole.Employee)]
        [HttpGet("GetReportsForCurrentUser")]
        public async Task<IEnumerable<ReportDto>> GetReportsForCurrentUserAsync()
        {
            return await _reportQueries.GetReportsForCurrentUserAsync();
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head)]
        [HttpGet("GetReportById")]
        public async Task<ReportDto> GetReportById(int id)
        {
            return await _reportQueries.GetReportById(id);
        }

        [AuthorizeRoles(CustomRole.Employee)]
        [HttpGet("GetReportByIdForCurrentUser")]
        public async Task<ReportDto> GetReportByIdForCurrentUserAsync(int id)
        {
            return await _reportQueries.GetReportByIdForCurrentUserAsync(id);
        }

        [AuthorizeRoles(CustomRole.Employee)]
        [HttpGet()]
        [Route("getselfreports")]
        public async Task<IActionResult> GetSelfReports([FromQuery] GetSelfDailyReportsDTO request)
        {
            return Ok(await _reportQueries.GetUserReportFilter(request));
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head, CustomRole.Employee)]
        [HttpGet("filter")]
        public async Task<IActionResult> GetReportFilter([FromQuery] DailyReportFilterDTO request)
        {
            return Ok(await _reportQueries.GetDailyReportFilter(request));
        }

        [HttpGet("downloadreports")]
        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head)]
        public async Task<IActionResult> DownloadFilteredReports([FromQuery] ExportExcelCommand request)
        {
            var data = await _mediator.Send(request);
            return File(data.FileStream, data.ContentType, data.FileName);
        }
    }
}